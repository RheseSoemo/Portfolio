# Home

My name is Rhese Soemo! I'm a software developer within the technology industry, currently located in Arizona.

I have extensive background in OOP and web development, particularly in the .NET framework and C# programming language, as well as a high level of competence in Java, Agile Scrum, N-Layer Architecture, PHP, JavaScript, and various other areas. I have had great experience in building software to regulation and compliance.

This [portfolio](https://rhese.gitbook.io/portfolio) is hosted on a [code repository](https://gitlab.com/RheseSoemo/Portfolio) with a CI/CD system on [GitLab](https://gitlab.com/) through the [GitBook](https://www.gitbook.com/) system.

## Who am I?

In short, I'm a software developer who loves building practical tech solutions to everyday problems.

Growing up in Washington with family in the industry, I was inundated with all kinds of technology from a very young age. I quickly developed a love for technology and a desire to find applications for it in my everyday life. I am passionate about using my skills to help both the technical and non-technical alike to build solutions that work for them.

I have always loved the hands on practical side of technology, so as I began to prepare for college, I began to look for degree programs that focused on the application of technology. I found Grand Canyon University's Bachelors of Science in Software Development to be very appealing, as most of the course work was practical classes on topics such as desktop software, frontend and backend webservers, mobile applications, and cloud-based infrastructure.

I joined the technology workforce shortly after I graduated with honors from GCU in April of 2023 and have continued to work to build my knowledge and effectiveness in technology, as well as look for ways to create technology solutions for everyday problems.

<div align="center"> <!-- So Image is centered on Local Rendering -->
  <figure> <!-- So Image is centered on GitBook Rendering -->
    <img src=".Assets/Overview-ItsMe.png" alt="A picture of me :D" width="375">
    <figcaption><p><em>Hey Look! It's Me!</em></p></figcaption>
  </figure>
</div>

### Technology Skill Sets

* **Languages:** C#, C++, C, CSS, HTML, Java, JavaScript, PHP, Python
* **Frameworks:** Angular, ASP.NET, Express, Maven, .NET, .NET Windows Forms, Node.js, Nodemon, React, Springboot
* **Data Storage:** JSON, LINQ, MongoDB, MySQL, PostgreSQL, YAML
* **IDEs:** CLion, Eclipse, Godot, IntelliJ, PHPStorm, Pycharm, Visual Studio, VSCode
* **Cloud Hosting Platforms:** AWS, Azure, Cloud Ways, Google Cloud, Heroku
* **Software Packages:** Atlassian, Docker, Git, Git Bash, GitHub Desktop, Kubernetes, MAMP, PenPot, Postman, Taiga, Adobe XD
* **Documentation:** ER Diagrams, Project Proposals, Project Requirements, Project Design, UML Diagrams, Site Map Diagram, Wireframes
* **Concepts:** ACID, Agile Scrum, BASE, Dependency Injection, N-Layer Architecture, Object Oriented Programming (OOP), REST API’s

### Links

* [LinkedIn](https://www.linkedin.com/in/rhese-soemo/)
  * [Resume (most up to date, available on LinkedIn)](https://www.linkedin.com/in/rhese-soemo/)
  * [LinkedIn Posts](https://www.linkedin.com/in/rhese-soemo/recent-activity/all/)
* [Git - browse my public projects](https://gitlab.com/RheseSoemo)
<!--* [YouTube - browse my random tech videos if available](https://www.youtube.com/@RheseSoemo)-->

## Portfolio Projects

1. [Personal Projects](./#personal-projects)
2. [Corporate Projects](./#corporate-projects)
3. [Academic Projects](./#academic-projects)
   * [Research and Development Projects](./#research-and-development-projects)
   * [In Class Projects](./#in-class-projects)

### Corporate Projects

I have worked on various projects as part of my technology career. For projects that I am not bound by NDA, have appropriate permission, and have the ability to show off a public frontend/release, I will add them to the list below. In each entry, I will add the employer that I developed the project under.

* [Automation Tooling @ Wells Fargo](Corporate/AutomationTooling.md) - In my spare time, built out tooling for non-technical teams to help them save time
* [Internal Company Podcast @ Wells Fargo](Corporate/InternalPodcast.md) - In my spare time, helped run a community organized internal technology weekly livestream
* [WordPress Websites](Corporate/WordpressProjects.md) - These projects were developed for clients while employed at Big Guy Digital as a website developer on the WordPress CMS.

### Personal Projects

As I have stated above, I have a deep passion for technology and using it to find solutions to everyday problems. Consequently, I am constantly coming up with ideas for new projects that will help solve problems. Many times, these projects fall outside my specific technological area of expertise, but that doesn't stop me.

I have many personal coding projects and codebases that I actively work on. If I believe that a project is in a state where it runs decently enough and could be helpful to others, I work to open source and make it public on my [GitLab](https://gitlab.com/RheseSoemo).

* [Online Portfolio](Personal/PersonalPortfolio.md) - Where you're at right now! A CI/CD pipelined system from GitLab to GitBook
* [Programming Teaching](Personal/ProgrammingTeaching.md) - Teaching family and friends technology and programming with easy to understand lessons
* [Game Development](Personal/GameDevelopment.md) - General experimenting or following step-by-step tutorials in order to understand how video games are made
* [Computer Construction](Personal/ComputerConstruction.md) - Assembling from scratch or modifying existing computer hardware systems for various purposes
* [Minecraft Server Administration](Personal/MinecraftServerAdministration.md) - Running a Minecraft server network for family and friends with a server-sided modding framework
  * [Null Core](Repository%20Facing/Personal/Null%20Core.md) - Created a library framework to quicken development of Minecraft Spigot mods

### Academic Projects

From 2020 to 2023 I earned my Bachelors of Science in Software Development at Grand Canyon University, also known as GCU. GCU's technology programs focuses on teaching in practical implementations. Their software development degree is no exception. In almost every class, I built a programming project from the ground up using the technology stack I was learning in the course. I also participated in extracurricular technology activities, primarily in GCU's Research Design Program, also known as RDP.

* [Senior Capstone Project](Academic/InClassProjects/SeniorCapstoneProject.md) - Built a GUI driven application on C#, .NET, and Avalonia UI to solve a problem that I found in the small business sector.

#### Research and Development Projects

* [AI-Enabled Medical Software](Repository%20Facing/Academic/RDP/AIMedicalSoftware.md) - Joined a team, led by Dr. Isac Artzi in building AI-Enabled Medical Software in Python

#### In Class Projects

_Please note that according to GCU's Technology College's Academic Policy, I am not allowed to publicly post repositories of my class work in order to combat cheating. Consequently, I can only show small code snippets and images of final submissions of programs and applications developed in GCU classrooms._

* [Social Media Blog](Academic/GrandCanyonUniversity/SocialMediaBlog.md) - Led a team in creating a social media blogging website utilizing PHP, JavaScript, MySQL, and Azure
* [Minesweeper Applications](Academic/GrandCanyonUniversity/MinesweeperApplications.md) - Designed and built Minesweeper using C# on the .NET Framework using Object Oriented Principals
