# Computer Construction

For almost ten years, I have run most of my daily personal computer activity off of a custom built PC. Starting as a fun side project with my father in highschool, I have since maintained and slowly upgraded my custom computer. I have also built and maintained small 24/7 server rigs with older pieces of spare hardware.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-ComputerConstruction-1_ComputerParts.png" alt="Boxes of Older Spare Hardware">
  <figcaption><p><em>Boxes of Older Spare Hardware</em></p></figcaption>
  </figure>
</div>

Originally, I researched computer parts fully by hand, going through the pain staking process of finding and writing down notes to confirm part compatibility. However, I eventually learned about [PC Part Picker](https://pcpartpicker.com/) which allowed me to simply choose parts and let the system tell me if they were compatible. Once I have a finished list of parts, I use general online stores like [Amazon](https://www.amazon.com/) and electronic-focused commerce sites like [New Egg](https://www.newegg.com/) to find the best deals to order the individual parts.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-ComputerConstruction-2_PcPartPicker.png" alt="My Rig on PC Part Picker">
  <figcaption><p><em>My Rig on PC Part Picker</em></p></figcaption>
  </figure>
</div>