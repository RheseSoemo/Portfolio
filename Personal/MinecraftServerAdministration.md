---
hidden: true
---

# Minecraft Server Administration

Since 2016, I have been administrating various Minecraft Servers, ranging from vanilla survival modes to creative mode to custom modes, like skyblock, and even to minigame systems. Along the way, I have built new processes, migrated servers, created best practices, and coded solutions for my small network of servers.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-MinecraftServerAdministration-1_PaperServer.png" alt="Screenshot of a currently active Minecraft server running 24 server-sided mods on version paper 1.20.1">
  <figcaption><p><em>A currently active Minecraft server running 24 with fully server-sided mods <br/>
Allowing users to have a modded experience without downloading any mods themselves</em></p></figcaption>
  </figure>
</div>

Currently, I run my entire server network on computers made from older physical hardware and parts. At times, I have run instances inside of partitioned cloud systems, however I found that it was cheaper and more effective to host locally.

All server instances use an extremely light-weight fork of Linux and are securely accessible through SSH, secure shell connection, or RDP, remote desktop protocol. This allows me to connect to the server and quickly run some commands in the terminal with SSH or complete complex operations more swiftly with a GUI with RDP.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-MinecraftServerAdministration-3_ServerFilesOnLinux.png" alt="A screenshot of a Minecraft server's files on Linux through a Remote Desktop connection">
  <figcaption><p><em>A Minecraft server's files on Linux viewable through a Remote Desktop Connection</em></p></figcaption>
  </figure>
</div>

Depending on the current needs of the active player-base on the network, multiple Minecraft server instances with vastly different experiences may be running. Without a proxying system, users cannot jump to different another Minecraft server without exiting the server and rejoining while using a different server address to connect to a separate port.

However, by adding a frontend proxy system, users can simply be sent from one server instance to another without manually going through the connection process. Currently, I'm using a fork, called Waterfall, of the open source system Bungeecord to act as the front end proxy that users can connect to.

Waterfall handles all of the authentication between the client, the server system, and the Minecraft login. When a player connects to the server, the proxy establishes a secure connection and ensures that the user is on a valid Minecraft account. It then internally routes the connection to the proper Minecraft server instance that the player is set to join.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-MinecraftServerAdministration-2_WaterfallProxyServer.png" alt="Screenshot of a currently active proxy server receiving initial information pings from Minecraft clients">
  <figcaption><p><em>A currently active proxy server receiving initial information pings from Minecraft clients</em></p></figcaption>
  </figure>
</div>

Recently, I have also begun to use source control to track configuration files for my various servers. This allows me to effectively backup all server configuration, track changes made over time, easily create forks for new servers, and easily develop a server in a development environment before deploying the new version in production.

Currently, I am using GitLab as version control for this project and am working to have individual server files split across various sections, in order to make it easier to save data.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-MinecraftServerAdministration-4_ServerConfigsOnGitLab.png" alt="Screenshot of a Minecraft server's configuration files loaded onto GitLab">
  <figcaption><p><em>A Minecraft server's configuration files loaded onto GitLab source control</em></p></figcaption>
  </figure>
</div>