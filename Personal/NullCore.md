---
hidden: true
---

# NullCore

As a software developer, I enjoy using my skills in circumstances around me. One of the ways I have found to do this is by coding mods for Minecraft. As I coded different projects, I found that I was reusing the same classes of code repeatably in each project and determined to create a solution. At first, I started designing my own version of the community's common fix for this problem by creating a library mod. However, I soon determined that this would be inefficient as having a library mod just for common functions did not make sense. It was also annoying to the user who would have to get a second mod in order for the first one to work. Consequently, I developed a dependency that could be loaded with either Maven or Gradle called Null Core.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-NullCore-1_Logo.png" alt="The Null Core Project's Logo">
  <figcaption><p><em>The Null Core Project's Logo</em></p></figcaption>
  </figure>
</div>

Null Core takes advantage of the open source project [JitPack](https://jitpack.io/) in order to allow developers to either use Maven or Gradle to import the framework into their project. I have written a comprehensive tutorial on the Project's GitHub page to walk developers through the steps of using JitPack to load Null Core.

Null Core features well documented and coded short cuts that allow coders to develop mods faster with the [Spigot](https://www.spigotmc.org/) modding framework. It features an extend version of the base main class that allows logging to be reached faster, messages sent to players with ease, and for the mod to be able to quickly set and get it's personal colored tag.

![Null Core's AugmentedJavaPlugin Class](../.Assets/Personal-NullCore-2\_ColorCodeTranslater.png)\
_Console Messenger Methods that Automatically Translate in-game Color Codes_

Null Core also features a storage type known as Plugin Configuration that allows developers to easily store and read their mod's settings. This allows programmers to save time by not coding out massive lists of variables, getters, and setters for global mod settings. Instead they can create a Plugin Configuration object and store their variables inside.

![Null Core's PluginConfiguration Class](../.Assets/Personal-NullCore-3\_CustomSettingStorage.png)\
_A Custom Setting Storage Type Known as PluginConfiguration_

If you wish to check out my work, progress, or code on Null Core, you can click [here](https://github.com/RT5Phantom/NullCore) for a link to the Null Core Project's repository.
