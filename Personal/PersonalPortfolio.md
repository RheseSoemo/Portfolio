# Online Portfolio
Originally created in March of 2022 on Github as a student at Grand Canyon University, I began to document academic, corporate, and personal projects on an online portfolio. I did this under the advice of my professors, who rightly pointed out that perspective employers would more likely take an interest in my resume if I had a portfolio linked.
<div align="center">
  <figure>
    <img src="../.Assets/Personal-Portfolio-1_OnRepo.png" alt="A screenshot of the first iteration of my portfolio, hosted on a git repository" width="375">
    <figcaption><p><em>A screenshot of the first iteration of my portfolio, hosted on a git repository</em></p></figcaption>
  </figure>
</div>

At first, I simply created markdown pages in a GitHub code repository and linked them together with a README.md. However, I soon began to desire a nicer looking and sleeker experience that only a website could offer. I decided that this would be an excellent opportunity to build a Continuous Integration/Continuous Delivery (aka CI/CD) pipeline that pulled data from a code repository and built it into a site. With this goal in mind I began to research ways to transform my portfolio to a full website.

After much researching and experimentation, I settled on using GitBook, a system that takes markdown files and builds them into a website. I then set to work to build the CI/CD pipeline system. After some work, I am now able to commit and push changes to my portfolio from my repository which then uses GitBook to build and then deploy my portfolio on a website.

<div align="center">
  <figure>
    <img width="500" src="../.Assets/Personal-Portfolio-2_OnWeb.png" alt="A screenshot of the second iteration of my portfolio, hosted on the web">
    <figcaption><p><em>A screenshot of the second iteration of my portfolio, hosted on the web</em></p></figcaption>
  </figure>
</div>