---
layout:
  title:
    visible: true
  description:
    visible: false
  tableOfContents:
    visible: true
  outline:
    visible: true
  pagination:
    visible: true
---

# Programming Teaching

It has been proven by science that one of the best ways to learn something is to teach it to someone else. Programming can be an extremely complex topic where basic knowledge is a pivotal foundation for further expertise down the line. Consequently, teaching the basics can be a difficult challenge.

Overtime, I have been creating, updating, and refining courses to try to help family and friends learn basic programming knowledge. It is a tricky balance to boil down the concepts to be simple enough for a non-technical person to understand but also create a strong foundational layer of knowledge that the student truly understands.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-SkillUpTraining-1_WhatArePrograms.png" alt="A slide asking what are programs, followed by an explanation">
  <figcaption><p><em>A slide explaining how programs work on a computer</em></p></figcaption>
  </figure>
</div>

A large portion of the development of this course has been me attempting to reverse engineer my own personal programming journey. I have been programming since before I was ten, but none of my knowledge really clicked in my head until my second semester in college.

I feel like a large reason for this was because concepts were explained improperly and way too simply, too complexly, or with the old fashion wave the hand and "if this doesn't make sense, that's okay!"

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-SkillUpTraining-2_SystemsAndTools.png" alt="A slide explaining the concept of systems and tools in programming">
  <figcaption><p><em>A slide explaining how classes/objects and methods/functions work in a simple to understand way.</em><br><em>The students aren't advanced to understand simple principals of OOP yet, but we need to lay the foundations in terms that they understand.</em></p></figcaption>
  </figure>
</div>

Building this course has been a slow journey of building and then refining topics, presentation slides, and coding projects, but has given me valuable insight into how non-technical people perceive and understand technology.

Though not my original intention, it has made me a better software developer in the corporate world. I have found that I can more easily breakdown complex technical software topics and explain it to customers, managers, scrum masters, and coworkers who have no software development experience.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-SkillUpTraining-3_WhyUseComments.png" alt="A slide asking and explaining why we should comments in our code">
  <figcaption><p><em>A slide demonstrating how extensive commenting can make really hard to read code understandable while encouraging students to comment their code so they can more easily wrap their heads around code</em></p></figcaption>
  </figure>
</div>
