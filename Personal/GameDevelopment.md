# Game Development
Growing up, I was always curious about how things were made and how they operate behind the scenes. My curiosity about game development was started when my dad sat me down in front of a modern computer that he gotten to run Quest for Glory 1 VGA. It immediately sparked an interest in how modern video games were developed and how older games used to be developed.

In the past, I have messed around non-seriously with the Unity and Unreal engines, but always had a hard time getting into them and had issues with the amount of processing power that they required. However, I eventually stumbled across the Godot game engine, which is open source and extremely lightweight.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-GameDevelopment-1_GodotEditor.png" alt="A game project open in the godot editor">
  <figcaption><p><em>A personal game project open in the Godot editor using a modded physics engine</em></p></figcaption>
  </figure>
</div>

I began to mess around with development inside the engine. Originally starting with scripting in C#, but quickly learning and moving over to developing using the built-in GDScript language, as it is a more efficient coding experience in the editor.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-GameDevelopment-2_DoomClone.png" alt="Doom clone in the godot engine">
  <figcaption><p><em>Doom clone in the godot engine</em></p></figcaption>
  </figure>
</div>

While tinkering around with personal projects and experiments, I also began following tutorials on how to make specific gaming experiences. Some of these tutorials were made in older versions of the engine which gave me a chance to find new ways to solve problems in the latest versions of Godot, as older systems had been deprecated or shifted around.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Personal-GameDevelopment-3_2DShooter.png" alt="Top down 2D shooter game">
  <figcaption><p><em>Top down 2D shooter game</em></p></figcaption>
  </figure>
</div>


## Public Game Repositories
- [Doom Clone - Miziziziz Tutorials](https://gitlab.com/RheseSoemo/miziziziztutorialstutorial-doomclone)
- [Tiled Dungeon Environment - Devlog Logan](https://gitlab.com/RheseSoemo/devloglogantutorial-tileddungeonenvironment)
- [Godot C# Mini Projects - Abdullah](https://gitlab.com/RheseSoemo/abdullah-godot-tutorials)