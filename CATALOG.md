# Catalog
* [Overview Readme](README.md)

## Rhese's Links
* [LinkedIn](https://www.linkedin.com/in/rhese-soemo/)
  * Resume Available
  * Bio Page Available

## Personal Projects
* [Online Portfolio](/Personal/PersonalPortfolio.md)
* [Programming Teaching](/Personal/ProgrammingTeaching.md)
* [Game Development](/Personal/GameDevelopment.md)
* [Computer Construction](/Personal/ComputerConstruction.md)
* Laser Association of Gaming
    * [Null Core](/Personal/NullCore.md)
    * [Minecraft Server Administration](/Personal/MinecraftServerAdministration.md)

## Corporate Projects
* [Automation Tooling @ Wells Fargo](/Corporate/AutomationTooling.md)
* [Internal Company Podcast @ Wells Fargo](/Corporate/InternalPodcast.md)
* [Wordpress Projects @ Big Guy Digital](/Corporate/WordpressProjects.md)

## Academic Projects
* [Research Development Projects](/README.md#research-and-development-projects)
    * [AI Medical Software](/Academic/ResearchAndDevelopment/MedicalAi.md)
    * [Academic Misuse of LLM AI](/Academic/ResearchAndDevelopment/AiWritingDetector.md)
* [In Class Projects](/README.md#in-class-projects)
    * [Senior Project: Overwatch Rest Reader](Academic/InClassProjects/SeniorCapstoneProject.md)
    * [Social Media Blogging Website](Academic/InClassProjects/SocialMediaBlog.md)
    * [Minesweeper Application](Academic/InClassProjects/MinesweeperApplications.md)