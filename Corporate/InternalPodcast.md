## Tech Tuesday @ Wells Fargo
As a software engineer at Wells Fargo, I volunteered spare time I had to help administrate an internal, grass-roots, technology podcast known as Tech Tuesday created by [Timithee Huggins](https://www.linkedin.com/in/timithee-huggins-15b48957/) and [Prashant Lande](https://www.linkedin.com/in/prashant-lande/).

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Corporate-InternalPodcast-1_TechTuesday.png" alt="Timithee Huggins pointing at the screen with a 'Live On Air' sign behind him">
  <figcaption><p><em>Timithee Huggins, one of the leaders of the community-led podcast, in his at home Tech Tuesday Studio</em></p></figcaption>
  </figure>
</div>

Tech Tuesday is hosted every Tuesday on a Teams call. As a volunteer, I was given the title of Ambassador and helped represent the show to the general employee community within the company. Weekly, I would attend the show and help administrate it in various ways. Whether I was answering questions, moderating chat, assisting in Q&A, or providing an example of enthusiastic show participation, I worked hard to assist the show in providing a great experience to its audience.

I quickly grew out of the standard role of Ambassador and began to take on extra responsibility when I could. I began to assist and then host entire segments on the broadcast, I co-produced then moved up to a regular rotation as a full producer, and I began to assist in editing videos. Through Tech Tuesday, I was given opportunities to assist in other internal live broadcasts, including company-wide Executive Towns and Q&A's.


>...Rhese Soemo has gone well above and beyond expectation in his support of the Tech Tuesday Podcast. He has consistently been an example to me and all of my colleagues. He has been a part of our Tech Tuesday Ambassadors team for 2024, and has helped to propel the podcast's success to nearly 6000 of our Wells Fargo Teammates all across Technology, and beyond. I have personally worked with him on multiple occasions, and he is constantly selfless, not afraid of challenges, and without regard for himself, leads the charge into the uncertainty of the technology realm. He is not afraid to do all that is needed for the success of any project he has been involved in. 
</br>
> -- <cite>Timithee Huggins, the head of Tech Tuesday, when speaking of me in a message to my leadership chain.</cite>