# Automation Tooling
## @ Wells Fargo
In my time working as a Software Engineer for Wells Fargo, I worked on many types of applications across my specific organization. In my org, the nature of our work meant that there was an ebb and flow to our everyday workloads. One week we would not have enough time to get everything done but the next week we would not have enough to do. 

As a result, managers over teams of developers banded together to assist each other. When one team didn't have enough time, developers from other teams would jump in and start assisting where they could. This loaning of engineering resources began to expand to other teams in my organization, who were not specifically engineering teams with software projects.

Through these circumstances, I was given the opportunity to assist non-technical teams in building out automated workflows that did not require manual human calculation. In at least one of these pre-automation projects, human error had caused miscalculations in a piece of our company strategy, as inaccurate data had been handed off to high-level executives. 

By eliminating the need for manual processes, navigation through systems, and calculating data, I was able to help free up coworkers' time for less tedious tasks, speed up processes, and precisely calculate data.

## @ Big Guy Digital
While at Big Guy Digital, I worked to automate report processes by coding JavaScript systems to automatically pull and compile data.