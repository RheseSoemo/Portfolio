# WordPress Website Development

## @ Big Guy Digital
As an employee of Big Guy Digital, I have worked on a variety of WordPress installations with different technologies. These technologies included GeneratePress, Gravity Forms, Rank Math, Yoast, Divi, GenerateBlocks, AAM, and WooCommerce.

Our company's primary technology was the GeneratePress builder which allowed a simple drag and drop block building interface in which to build websites. This allowed us to spin up a site, build it out, deploy it, and give access to our clients who could easily modify or create new pages.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Academic-WordpressProjects-1_LawyerSite.png" alt="Building a Lawyer's Website with GeneratePress">
  <figcaption><p><em>Building a Lawyer's Website with GeneratePress Technology</em></p></figcaption>
  </figure>
</div>

During my time developing WordPress websites, I worked directly with clients and their graphic designers to build websites. I have learned many valuable lessons on how to interact with customers in order to achieve their needs. Each client whose project was put under my development had completly different needs than any before and consequently required me to be constantly learning new technologies and development strategies.

<div align="center">
  <figure>
  <img width="500" src="../.Assets/Academic-WordpressProjects-2_NoteServicingSite.png" alt="Deployed Note Servicing Website">
  <figcaption><p><em>Deployed Note Servicing Website</em></p></figcaption>
  </figure>
</div>