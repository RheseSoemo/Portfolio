---
hidden: true
---

# Minesweeper Applications

As a part of my CST-227 Enterprise Applications Programming II course, I developed a fully functional Minesweeper game in C#. This project went through several iterations as I progressed through the course.

As part of this project, I created a backend Minesweeper library that I could implement into multiple project in order to easily create different versions of minesweeper. For the first phase of this project, I developed the backend library while hooking it up to a console application on the frontend.

<div align="center">
  <figure>
	<img width="500" src="../../.Assets/Academic-GrandCanyonUniversity-Minesweeper-1.png" alt="A screenshot of a console based minesweeper game">
	<figcaption><p><em>Console Version of the Minesweeper Game</em></p></figcaption>
  </figure>
</div>

I created two main objects in the Minesweeper library, the Cell object and the Board object. These objects worked on the backend to create a gameboard, populate it with cells, let the user reveal a cell to be clear or a bomb, let the user flag a cell, and end the game in victory or defeat.

```csharp
//===================================//
//==========Class=Variables==========//
//===================================//
//Cells position in column and row
public int cellRow { get; set; } = -1;
public int cellColumn { get; set; } = -1;

//If the cell is a mine, replacement for "live" value
public Boolean isMine { get; set; }

//Is this cell flagged by the player
public Boolean isFlagged { get; set; } = false;

//If the cell has been checked by the player, replacement for "visited" value
public Boolean hasBeenChecked { get; set; } = false;

//How many mines are touching this cell, replacement for the neighboring "live" cells
public int numberOfMinesTouching { get; set; } = 0;
```
<div align="center">
<figure>
	<figcaption><p><em>Cell Object's Class Variables in the Minesweeper Library</em></p></figcaption>
</figure>
</div>

In Minesweeper, if a player clicks on a non-bomb cell, the game will then clear all non-bomb cells in every direction, until it hits a non-bomb cell that is touching a bomb. This feature is also known as flood fill. I worked to build a flood fill system that used recursive logic that revealed non-bomb cells to a player. This system required a lot of fine-tuning to perfect, as this was my first serious dive into the topic of recursion and it took me a little bit to fully grasp the concept.

> "To understand recursion, you must know recursion."\
> \- [A Very Wise Man](https://www.linkedin.com/in/michael-sarlo-b48439263/)

```csharp
//==============================//
//==========Flood=Fill==========//
//==============================//
private void floodFill(int r, int c)
{
	boardGrid[r, c].hasBeenChecked = true;  //Saying it has been checked

	if (boardGrid[r, c].numberOfMinesTouching == 0)
	{
		//Northern Check
		if (isValid(r + 1, c) == true &&
		boardGrid[r + 1, c].isMine == false &&
		boardGrid[r + 1, c].hasBeenChecked == false)
		{
			floodFill(r + 1, c);
		}
		...
	}
}
```
<div align="center">
<figure>
	<figcaption><p><em>The Flood Fill Method in the Board Object Class that Recursively Revealed all Cells that were not Bombs</em></p></figcaption>
</figure>
</div>

After developing the main functions of the Minesweeper Library, I then worked to implement it into a Windows Form Application. This allowed me to drastically improve the user experience with the game, as now the user could easily click buttons instead of typing out commands to a command console.

<div align="center">
  <figure>
	<img width="500" src="../../.Assets/Academic-GrandCanyonUniversity-Minesweeper-2.png" alt="A screenshot of minesweeper on a window">
	<figcaption><p><em>Windows Form Version of the Minesweeper Game</em></p></figcaption>
  </figure>
</div>

The implementation of the Minesweeper library into Windows Forms was interesting, as I needed to create a frontend button that was attached to the backend Minesweeper Cell Object being used to run the game. To mirror more effectively, I created an update board view method that was called every time the process of revealing a cell, revealing all bombs, or flagging a cell ended.

```csharp
//====Creating=Board====//
gameBoard = new Board(futureBoardSize, futureBoardDifficulty);  //Instantiating new board

//====Creating=Board=View====//
buttonGrid = new Button[futureBoardSize, futureBoardSize];  //Instantiating a new button grid
```
<div align="center">
<figure>
	<figcaption><p><em>Instantiating a Board Object and a Button Grid Object to Mirror It</em></p></figcaption>
</figure>
</div>


An extra feature I added onto the windows application was the addition of score tracking. This allowed users to enter in their username, play a game, and then have their score recorded. I also built the score viewer so that players could see the scores for different difficulties and board sizes.

<div align="center">
  <figure>
	<img width="500" src="../../.Assets/Academic-GrandCanyonUniversity-Minesweeper-3.png" alt="A window showing high scores and difficulty selection">
	<figcaption><p><em>Cataloged Scores of Board Size of 1 and Difficulty of 1</em></p></figcaption>
  </figure>
</div>