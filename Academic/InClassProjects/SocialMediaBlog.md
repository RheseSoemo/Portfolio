---
hidden: true
---

# Social Media Blog

As part of my CST-126 Database Application Programming 1, I worked with a group of members to create a social media blogging website, called BragBook. This group project presented many unique challenges.

Firstly, our team was bigger than standard. Technically, each project team within in the class was supposed to have three members. However, when I went to view my teammates, instead three of us there was a total of five team members.

This would have made the project much easier, except for the second challenge of the fact that we were taking the course online. Additionally, we were all in different time zones. This made communication hard and generally slowed down project development.

The third major challenge of the project was that each one of the project members were in vastly different walks of life. As a full time student, my life was in complete contrast to my project teammates. One was actively in the military and getting his technology degree on the side, the second was an information technology professional looking to move into software development, while my third and fourth teammates were looking to move into the technology sphere.

<div align="center">
  <figure>
	<img width="500" src="../../.Assets/Academic-GrandCanyonUniversity-SocialMediaBlog-1-Login.png" alt="Login Page of the Social Media Blogging Website">
	<figcaption><p><em>Login Page of the Social Media Blogging Website</em></p></figcaption>
  </figure>
</div>

These challenges made it difficult to work together, as we all tried to find meeting times to discuss our next steps or record a video to show off to our instructor the latest changes in the project. Adding on to all of this was the fact that the whole team was learning on the go and did not know fully how to work the varying programming languages. There was also a large amount of drama that was triggered from misunderstandings between project members. As the elected leader of the project, I took it upon myself to smooth out these differences by being a peacemaker. It was a long and arduous project, however we were able to create a project that I am proud of.

<div align="center">
  <figure>
	<img width="500" src="../../.Assets/Academic-GrandCanyonUniversity-SocialMediaBlog-2-UserPosts.png" alt="Viewing Blog Posts which linked Authors, Comments, Likes, as well as Editing and Deleting Capabilities for the Poster and Site Administrators">
	<figcaption><p><em>Viewing Blog Posts which linked Authors, Comments, Likes, as well as Editing and Deleting Capabilities for the Poster and Site Administrators</em></p></figcaption>
  </figure>
</div>

In the end of this project, we had used PHP, JavaScript, HTML, CSS, and MySQL to create a functioning social media blogging site, which included user registration, login, roles, content creation, ratings, and comments.

<div align="center">
  <figure>
	<img width="500" src="../../.Assets/Academic-GrandCanyonUniversity-SocialMediaBlog-3-AdminDashboard.png" alt="Login Page of the Social Media Blogging Website">
	<figcaption><p><em>Login Page of the Social Media Blogging Website</em></p></figcaption>
  </figure>
</div>