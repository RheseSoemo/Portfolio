# Table of contents
* [Home](README.md)
* [LinkedIn](https://www.linkedin.com/in/rhese-soemo/)

## Personal Projects

* [Online Portfolio](Personal/PersonalPortfolio.md)
* [Programming Teaching](Personal/ProgrammingTeaching.md)
* [Game Development](Personal/GameDevelopment.md)
* [Computer Construction](Personal/ComputerConstruction.md)
* [Minecraft Server Administration](Personal/MinecraftServerAdministration.md)
  * [NullCore](Personal/NullCore.md)

## Corporate Projects
* [Automation Tooling @ Wells Fargo](Corporate/AutomationTooling.md)
* [Internal Company Podcast @ Wells Fargo](Corporate/InternalPodcast.md)
* [Wordpress Projects @ Big Guy Digital](Corporate/WordpressProjects.md)

## Academic Projects
* [AI Medical Software](Academic/ResearchAndDevelopment/MedicalAi.md)
* [Academic Misuse of LLM AI](Academic/ResearchAndDevelopment/AiWritingDetector.md)
* [Senior Project: Overwatch Rest Reader](Academic/InClassProjects/SeniorCapstoneProject.md)
* [Minesweeper Applications](Academic/InClassProjects/MinesweeperApplications.md)
* [Social Media Blog](Academic/InClassProjects/SocialMediaBlog.md)

## More
* [Full Portfolio Catalog](CATALOG.md)
* [GitLab](https://gitlab.com/RheseSoemo)
<!-- * [HackerRank](https://www.hackerrank.com/profile/rhese_soemo)-->
<!-- * [GitHub](https://github.com/RheseSoemo)-->